<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Properties extends Model
{
    protected $table = 'properties';

    protected $fillable = ['user_id','property_name','property_type','property_purpose','sale_price','rent_price','address','map_latitude','map_longitude','bathrooms','bedrooms','area','description','featured_image'];

	public function scopeSearchByKeyword($query, $terms,$type,$purpose,$price,$min_price,$max_price)
    {
        if ($terms != ''){
            $query->where(function ($query) use ($terms) {
                $query->whereHas('city', function ($query) use ($terms) {
                        $query->where('cities.name','LIKE', '%' . $terms . '%');
                    });
            })->orWhere('property_name', 'Like', '%' . $terms . '%');
            //dd($query);
        }
        if ($min_price != ''){
            $query->where(function ($query) use ($min_price,$price) {
                $query->whereRaw("$price > $min_price");
            });
        }
        if ($min_price != ''){
            $query->where(function ($query) use ($max_price,$price) {
                $query->whereRaw("$price > $max_price");
            });
        }

        //dd($purpose);
        if ($purpose != ''){
            $query->where(function ($query) use ($purpose) {
                $query->where('property_purpose', $purpose);
            });
        }

        if ($type != ''){
            $query->where(function ($query) use ($type) {
                $query->where('property_type', $type);
            });
        }

        //print_r($query);
        return $query;
    }

    public function type()
    {
        return $this->belongsTo(Types::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

}
