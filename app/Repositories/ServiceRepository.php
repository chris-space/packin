<?php
/**
 * Created by PhpStorm.
 * @author Abiye Chris. I. Surulere < suruabiye@gmail.com >
 * Date: 7/7/2020
 * Time: 11:34 PM
 */

namespace App\Repositories;


use App\Service;

class ServiceRepository
{
    public function create($data)
    {
        $service = new Service();
        $service->name = $data->name;
        $service->font_icon = $data->font_icon;
        $service->description = $data->description;
        $service->save();

        return $service;
    }
}
