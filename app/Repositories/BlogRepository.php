<?php
/**
 * Created by PhpStorm.
 * @author Abiye Chris. I. Surulere < suruabiye@gmail.com >
 * Date: 7/13/2020
 * Time: 4:55 PM
 */

namespace App\Repositories;


use App\Blog;

class BlogRepository
{
    public function store($data)
    {
        $blog = new Blog();
        $blog->title = $data->title;
        $blog->content = $data->content;
        $blog->author = $data->author;
        $blog->slug = $data->slug;
        $blog->status = $data->status;
        $blog->save();
    }

    public function update($data, $slug)
    {
        $blog = Blog::where('slug', $slug)->first();
        $blog->title = $data->title;
        $blog->content = $data->content;
        $blog->author = $data->author;
        $blog->slug = $data->slug;
        $blog->status = $data->status;
        $blog->save();
    }

}
