<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Repositories\BlogRepository;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        return view('blog.index');
    }

    public function create()
    {
        return view('blog.create');
    }

    public function store(BlogRequest $request, BlogRepository $blogRepository)
    {
        return redirect()->back()->with('status','Blog created');
    }

    public function show($id)
    {
        return view('blog.show');
    }

    public function edit($id)
    {
        return view('blog.edit');
    }

    public function update(BlogRequest $request, BlogRepository $blogRepository, $id)
    {
        return redirect()->back()->with('status','Blog created');
    }
}
