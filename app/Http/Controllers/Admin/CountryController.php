<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Session;
use App\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CountryController extends MainAdminController
{
    public function __construct()
    {
        $this->middleware('auth');

        parent::__construct();

    }

    public function index()
    {
        $countries = Country::orderBy('name','asc')->get();

        return view('admin.pages.country.countries',compact('countries'));
    }

    public function create()    {

        if(Auth::User()->usertype!="Admin"){
            Session::flash('flash_message', 'Access denied!');
            return redirect('admin/dashboard');
        }
        return view('admin.pages.country.create_edit_country');
    }

    public function edit($id)
    {
        if(Auth::User()->usertype!="Admin"){
            Session::flash('flash_message', 'Access denied!');
            return redirect('admin/dashboard');
        }
        $country = Country::findOrFail($id);
        return view('admin.pages.country.create_edit_country',compact('country'));
    }

    public function storeOrUpdate(Request $request)
    {
        $rule=array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator = Validator::make($request->except('token'),$rule);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator->messages());
        }

        if(!empty($request->id)){
            $country = Country::findOrFail($request->id);
        }else{
            $country = new Country();
        }
        $country->name = $request->name;
        $country->status = $request->status;
        $country->save();

        if(!empty($request->id)){
            Session::flash('flash_message', 'Changes Saved');
            return redirect()->back();
        }

        Session::flash('flash_message', 'Added');
        return redirect()->back();
    }

    public function destroy($id)
    {
        if(Auth::User()->usertype!="Admin"){
            Session::flash('flash_message', 'Access denied!');
            return redirect('admin/dashboard');
        }
        $country = Country::findOrFail($id);
        $country->delete();
        Session::flash('flash_message', 'Deleted');
        return redirect()->back();
    }

    public function status($id)
    {
        $country = Country::findOrFail($id);
        if(Auth::User()->usertype!="Admin")
        {
            Session::flash('flash_message', 'Access denied!');
            return redirect('admin/dashboard');
        }
        if($country->status === 'active')
        {
            $country->status = 'inactive';
            $country->save();
            Session::flash('flash_message', 'Unpublished');
        }
        else
        {
            $country->status = 'active';
            $country->save();
            Session::flash('flash_message', 'Published');
        }
        return redirect()->back();
    }
}
