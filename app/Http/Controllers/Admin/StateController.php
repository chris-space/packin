<?php

namespace App\Http\Controllers\Admin;

use Session;
use Auth;
use Validator;
use App\Country;
use App\Http\Controllers\Controller;
use App\State;
use Illuminate\Http\Request;

class StateController extends MainAdminController
{
    public function __construct()
    {
        $this->middleware('auth');

        parent::__construct();

    }

    public function index()
    {
        $states = State::orderBy('name','asc')->get();

        return view('admin.pages.state.states', [
            'states' => $states,
        ]);
    }

    public function create()    {
        if(Auth::User()->usertype!="Admin"){
            Session::flash('flash_message', 'Access denied!');
            return redirect('admin/dashboard');
        }
        return view('admin.pages.state.create_edit_state',[
            'countries' => Country::orderBy('name')->get(),
        ]);
    }

    public function edit($id)
    {
        if(Auth::User()->usertype!="Admin"){
            Session::flash('flash_message', 'Access denied!');
            return redirect('admin/dashboard');
        }

        return view('admin.pages.state.create_edit_state', [
            'state' => State::findOrFail($id),
            'countries' => Country::orderBy('name')->get(),
        ]);
    }

    public function storeOrUpdate(Request $request)
    {
        $rule=array(
            'name' => 'required',
            'country' => 'required',
            'status' => 'required',
        );

        $validator = Validator::make($request->except('token'),$rule);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator->messages());
        }

        if(!empty($request->id)){
            $state = State::findOrFail($request->id);
        }else{
            $state = new State();
        }
        $state->name = $request->name;
        $state->country_id = $request->country;
        $state->status = $request->status;
        $state->save();

        if(!empty($request->id)){
            Session::flash('flash_message', 'Changes Saved');
            return redirect()->back();
        }

        Session::flash('flash_message', 'Added');
        return redirect()->back();
    }

    public function status($id)
    {
        $state = State::findOrFail($id);
        if(Auth::User()->usertype!="Admin")
        {
            Session::flash('flash_message', 'Access denied!');
            return redirect('admin/dashboard');
        }
        if($state->status === 'active')
        {
            $state->status = 'inactive';
            $state->save();
            Session::flash('flash_message', 'Unpublished');
        }
        else
        {
            $state->status = 'active';
            $state->save();
            Session::flash('flash_message', 'Published');
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        if(Auth::User()->usertype!="Admin"){
            Session::flash('flash_message', 'Access denied!');
            return redirect('admin/dashboard');
        }
        $state = State::findOrFail($id);
        $state->delete();
        Session::flash('flash_message', 'Deleted');
        return redirect()->back();
    }
}
