<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\State;
use Auth;
use App\User;
use App\City;
use Validator;
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CityController extends MainAdminController
{
	public function __construct()
    {
		 $this->middleware('auth');

		 parent::__construct();

    }
    public function index()
    {
    	$cities = City::orderBy('name')->get();

        return view('admin.pages.city.cities',compact('cities'));
    }

	 public function create()    {

        if(Auth::User()->usertype!="Admin"){

            Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');

        }

        return view('admin.pages.city.create_edit_city', [
            'countries' => Country::orderBy('name')->get(),
        ]);
    }

    public function edit($id)
    {
        if(Auth::User()->usertype!="Admin"){
            Session::flash('flash_message', 'Access denied!');
            return redirect('admin/dashboard');
        }

        return view('admin.pages.city.create_edit_city', [
            'city' => City::findOrFail($id),
            'countries' => Country::orderBy('name')->get(),
        ]);
    }

    public function storeOrUpdate(Request $request)
    {
        $rule=array(
            'name' => 'required',
            'country' => 'required',
            'status' => 'required',
        );

        $validator = Validator::make($request->except('token'),$rule);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator->messages());
        }

        if(!empty($request->id)){
            $city = City::findOrFail($request->id);
        }else{
            $city = new City();
        }
        $city->name = $request->name;
        $city->state_id = $request->state;
        $city->status = $request->status;
        $city->save();

        if(!empty($request->id)){
            Session::flash('flash_message', 'Changes Saved');
            return redirect()->back();
        }

        Session::flash('flash_message', 'Added');
        return redirect()->back();
    }

    public function delete($id)
    {

    	if(Auth::User()->usertype!="Admin"){

            Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');

        }

        $city = City::findOrFail($id);

		$city->delete();

        Session::flash('flash_message', 'Deleted');

        return redirect()->back();

    }

    public function status($id)
    {
        $city = City::findOrFail($id);

       	if(Auth::User()->usertype!="Admin")
       	{

            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');

        }

		if($city->status==1)
		{
			$city->status='0';
	   		$city->save();

	   		\Session::flash('flash_message', 'Unpublished');
		}
		else
		{
			$city->status='1';
	   		$city->save();

	   		\Session::flash('flash_message', 'Published');
		}

        return redirect()->back();

    }

}
