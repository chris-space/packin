<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\State;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getStateByCountryId($cid)
    {
        $state = State::orderBy('name')->where('country_id', $cid)->get();
        return response()->json($state);
    }
}
