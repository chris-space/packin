<?php

namespace App\Http\Controllers;

use App\Properties;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function filter(Request $request, Properties $property)
    {
        // Search for a user based on their name.
        if ($request->has('terms')) {
            return $property->whereHas('name', $request->input('name'))->get();
        }
    }
}
