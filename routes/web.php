<?php

Route::get('/test', function (){
    return view('home');
});

// FRont pages
Route::get('/', 'IndexController@index')->name('home');
Route::get('about-us', 'IndexController@aboutus_page')->name('about-us');
Route::get('careers-with-us', 'IndexController@careers_with_page')->name('careers');
Route::get('terms-conditions', 'IndexController@terms_conditions_page')->name('terms');
Route::get('privacy-policy', 'IndexController@privacy_policy_page')->name('privacy-policy');
Route::get('contact-us', 'IndexController@contact_us_page')->name('contact-us');
Route::post('contact-us', 'IndexController@contact_us_sendemail')->name('contact-us');
Route::post('subscribe', 'IndexController@subscribe')->name('subscribe');

Route::get('agents', 'AgentsController@index')->name('agents');
Route::get('builders', 'AgentsController@builder_list')->name('builders');
Route::get('featured', 'PropertiesController@featuredproperties')->name('featured');
Route::get('sale', 'PropertiesController@saleproperties')->name('sale');
Route::get('rent', 'PropertiesController@rentproperties')->name('rent');
Route::get('properties', 'PropertiesController@index')->name('properties');
Route::get('properties/{slug}', 'PropertiesController@propertysingle')->name('property.view');
Route::get('type/{slug}', 'PropertiesController@propertiesbytype')->name('property.type');
Route::post('agentscontact', 'PropertiesController@agentscontact')->name('agent.contact');
Route::get('search-properties', 'PropertiesController@searchproperties')->name('property.search');
Route::post('search-properties', 'PropertiesController@searchproperties')->name('property.search');
Route::post('search', 'PropertiesController@searchkeywordproperties')->name('property.search.keyword');

//Authentication
Route::get('login', 'IndexController@login')->name('login');
Route::post('login', 'IndexController@postLogin')->name('login');
Route::get('register', 'IndexController@register')->name('register');
Route::post('register', 'IndexController@postRegister')->name('register');

Route::get('logout', 'IndexController@logout')->name('logout');

// Password reset link request routes...
Route::get('admin/password/email', 'Auth\PasswordController@getEmail')->name('password.rest');
Route::post('admin/password/email', 'Auth\PasswordController@postEmail')->name('password.reset');

// Password reset routes...
Route::get('admin/password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('admin/password/reset', 'Auth\PasswordController@postReset');

Route::get('auth/confirm/{code}', 'IndexController@confirm');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {

    Route::get('/', 'IndexController@index')->name('admin.dashboard');
    Route::post('login', 'IndexController@postLogin')->name('admin.login');
    Route::get('logout', 'IndexController@logout')->name('admin.logout');

    Route::get('dashboard', 'DashboardController@index')->name('admin.home');
    Route::get('profile', 'AdminController@profile')->name('admin.profile');
    Route::post('profile', 'AdminController@updateProfile')->name('admin.profile.update');
    Route::post('profile_pass', 'AdminController@updatePassword')->name('admin.profile.password.change');
    Route::get('settings', 'SettingsController@settings')->name('admin.settings');
    Route::post('settings', 'SettingsController@settingsUpdates')->name('admin.settings.update');
    Route::post('social_links', 'SettingsController@social_links_update')->name('admin.social.links');
    Route::post('addthisdisqus', 'SettingsController@addthisdisqus');
    Route::post('about_us', 'SettingsController@about_us_page');
    Route::post('careers_with_us', 'SettingsController@careers_with_us_page');
    Route::post('terms_conditions', 'SettingsController@terms_conditions_page');
    Route::post('privacy_policy', 'SettingsController@privacy_policy_page');
    Route::post('headfootupdate', 'SettingsController@headfootupdate');

    //Slider
    Route::get('slider', 'SliderController@sliderlist');
    Route::get('slider/addslide', 'SliderController@addeditSlide');
    Route::post('slider/addslide', 'SliderController@addnew');
    Route::get('slider/addslide/{id}', 'SliderController@editSlide');
    Route::get('slider/delete/{id}', 'SliderController@delete');


    Route::get('testimonials', 'TestimonialsController@testimonialslist');
    Route::get('testimonials/addtestimonial', 'TestimonialsController@addeditestimonials');
    Route::post('testimonials/addtestimonial', 'TestimonialsController@addnew');
    Route::get('testimonials/addtestimonial/{id}', 'TestimonialsController@edittestimonial');
    Route::get('testimonials/delete/{id}', 'TestimonialsController@delete');


    Route::prefix('properties')->group(function (){
        Route::get('/', 'PropertiesController@propertieslist')->name('admin.property');
        Route::get('/addproperty', 'PropertiesController@addeditproperty')->name('admin.property.create');
        Route::post('/addproperty', 'PropertiesController@addnew')->name('admin.property.store');
        Route::get('/addproperty/{id}', 'PropertiesController@editproperty')->name('admin.property.edit');
        Route::get('/status/{id}', 'PropertiesController@status')->name('admin.property.status');
    });

    Route::get('properties/featuredproperty/{id}', 'PropertiesController@featuredproperty');
    Route::get('properties/delete/{id}', 'PropertiesController@delete');
    Route::get('featuredproperties', 'FeaturedPropertiesController@propertieslist');


    Route::get('users', 'UsersController@userslist');
    Route::get('users/adduser', 'UsersController@addeditUser');
    Route::post('users/adduser', 'UsersController@addnew');
    Route::get('users/adduser/{id}', 'UsersController@editUser');
    Route::get('users/delete/{id}', 'UsersController@delete');

    Route::prefix('countries')->group(function (){
        Route::get('/', 'CountryController@index')->name('admin.country');
        Route::get('/create', 'CountryController@create')->name('admin.country.create');
        Route::get('/{id}/edit', 'CountryController@edit')->name('admin.country.edit');
        Route::post('/store', 'CountryController@storeOrUpdate')->name('admin.country.storeOrUpdate');
        Route::get('/{id}/delete', 'CountryController@destroy')->name('admin.country.delete');
        Route::get('/{id}/status', 'CountryController@status')->name('admin.country.status');
    });

    Route::prefix('states')->group(function (){
        Route::get('/', 'StateController@index')->name('admin.state');
        Route::get('/create', 'StateController@create')->name('admin.state.create');
        Route::get('/{id}/edit', 'StateController@edit')->name('admin.state.edit');
        Route::post('/store', 'StateController@storeOrUpdate')->name('admin.state.storeOrUpdate');
        Route::get('/{id}/delete', 'StateController@destroy')->name('admin.state.delete');
        Route::get('/{id}/status', 'StateController@status')->name('admin.state.status');
    });

    Route::prefix('cities')->group(function (){
        Route::get('/', 'CityController@index')->name('admin.city');
        Route::get('/create', 'CityController@create')->name('admin.city.create');
        Route::get('/{id}/edit', 'CityController@edit')->name('admin.city.edit');
        Route::post('/store-and-update', 'CityController@storeOrUpdate')->name('admin.city.storeOrUpdate');
        Route::get('/{id}/delete', 'CityController@destroy')->name('admin.city.delete');
        Route::get('/{id}/status', 'CityController@status')->name('admin.city.status');
    });

    Route::get('subscriber', 'SubscriberController@subscriberlist');
    Route::get('subscriber/delete/{id}', 'SubscriberController@delete');

    Route::get('partners', 'PartnersController@partnerslist');
    Route::get('partners/addpartners', 'PartnersController@addpartners');
    Route::post('partners/addpartners', 'PartnersController@addnew');
    Route::get('partners/addpartners/{id}', 'PartnersController@editpartners');
    Route::get('partners/delete/{id}', 'PartnersController@delete');

    Route::get('inquiries', 'InquiriesController@inquirieslist');
    Route::get('inquiries/delete/{id}', 'InquiriesController@delete');


    Route::get('types', 'TypesController@typeslist');
    Route::get('types/addtypes', 'TypesController@addedittypes');
    Route::post('types/addtypes', 'TypesController@addnew');
    Route::get('types/addtypes/{id}', 'TypesController@edittypes');
    Route::get('types/delete/{id}', 'TypesController@delete');

});
