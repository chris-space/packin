<?php

use App\Events\Inst;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        // Create admin account
        DB::table('users')->insert([
            'usertype' => 'Admin',
            'name' => 'Chris',
            'email' => 'suruabiye@gmail.com',
            'password' => bcrypt('chris1234**'),
            'image_icon' => null,
            'remember_token' => Str::random(10),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);



        DB::table('settings')->insert([
            'site_style' => 'blue',
            'site_name' => 'Packin - Real Estate',
            'site_email' => 'suruabiye@gmail.com',
            'site_logo' => 'logo.png',
            'site_favicon' => 'favicon.png',
            'site_description' => 'Real Estate business',
            'site_copyright' => 'Copyright © 2020 Packin. All rights reserved.'
        ]);


       // factory('App\User', 20)->create();
    }
}
