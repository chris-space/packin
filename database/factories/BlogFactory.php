<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(10),
        'author' => 1,
        'content' => $faker->words(300),
        'status' => 'active',
    ];
});
