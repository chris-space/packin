@extends("layouts.app")
@section("content")

    <!-- Banner
    ================================================== -->
    <div class="parallax" data-background="{{ asset('images/home-parallax.jpg') }}" data-color="#36383e" data-color-opacity="0.45" data-img-width="2500" data-img-height="1600">
        <div class="parallax-content">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Main Search Container -->
                        @include("_particles.slidersearch")
                        <!-- Main Search Container / End -->
                    </div>
                </div>
            </div>

        </div>
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <h3 class="headline margin-bottom-25 margin-top-65">Newly Added</h3>
            </div>

            <!-- Carousel -->
            <div class="col-md-12">
                <div class="carousel">

                @if(isset($properties))
                    @foreach($properties as $property)
                        <!-- Listing Item -->
                        <div class="carousel-item">
                                <div class="listing-item">

                                    <a href="{{ route('property.view', ['slug' => $property->property_slug]) }}" class="listing-img-container">

                                        <div class="listing-badges">
                                            @if($property->featured_property === 1)
                                                <span class="featured">Featured</span>
                                            @endif
                                            <span>For {{$property->property_purpose}}</span>
                                        </div>

                                        <div class="listing-img-content">
                                            <span class="listing-price">{{env('Currency_Code')}} @if($property->property_purpose === 'Sale') {{ number_format((float)$property->sale_price) }} @else {{ number_format((float)$property->rent_price) }} @endif</i></span>
                                            <span class="like-icon with-tip" data-tip-content="Add to Bookmarks"></span>
                                        </div>

                                        <div class="listing-carousel">
                                            @if($property->property_images1)
                                                <div><img src="{{ asset('upload/properties/'.$property->property_images1.'-b.jpg') }}" alt=""></div>
                                            @endif

                                            @if($property->property_images2)
                                                <div class="item"><img src="{{ asset('upload/properties/'.$property->property_images2.'-b.jpg') }}" alt=""></div>
                                            @endif
                                        </div>

                                    </a>

                                    <div class="listing-content">

                                        <div class="listing-title">
                                            <h4><a href="{{ route('property.view', ['slug' => $property->property_slug]) }}">{{ Str::limit($property->property_name,35) }}</a></h4>
                                            <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
                                                <i class="fa fa-map-marker"></i>
                                                {{ Str::limit($property->address,40) }}
                                            </a>
                                        </div>

                                        <ul class="listing-features">
                                            @if($property->area)
                                                <li>Area <span>{{$property->area}} </span></li>
                                            @endif
                                            @if($property->bedrooms)
                                                <li>Bedrooms <span>{{$property->bedrooms}}</span></li>
                                             @endif
                                            @if($property->bathrooms)
                                                <li>Bathrooms <span>{{$property->bathrooms}}</span></li>
                                             @endif
                                        </ul>

                                        <div class="listing-footer">
                                            <a href="#"><i class="fa fa-user"></i> {{ $property->user->name }}</a>
                                            <span><i class="fa fa-calendar-o"></i> {{ $property->created_at->diffForHumans() }}</span>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        <!-- Listing Item / End -->
                    @endforeach
                @endif

                </div>
            </div>
            <!-- Carousel / End -->

        </div>
    </div>



    <!-- Fullwidth Section -->
    <section class="fullwidth margin-top-105" data-background-color="#f7f7f7">

        <!-- Box Headline -->
        <h3 class="headline-box">What are you looking for?</h3>

        <!-- Content -->
        <div class="container">
            <div class="row">

                <div class="col-md-6 col-sm-6">
                    <!-- Icon Box -->
                    <div class="icon-box-1">

                        <div class="icon-container">
                            <i class="im im-icon-Office"></i>
                            <div class="icon-links">
                                <a href="{{ route('sale') }}">For Sale</a>
                                <a href="{{ route('rent') }}">For Rent</a>
                            </div>
                        </div>

                        <h3>Private Home Office</h3>
                        <p>
                            Finding the best and most appropriate apartment fit for you is just a click away.
                        </p>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <!-- Icon Box -->
                    <div class="icon-box-1">

                        <div class="icon-container">
                            <i class="im im-icon-Home-2"></i>
                            <div class="icon-links">
                                <a href="{{ route('sale') }}">For Sale</a>
                                <a href="{{ route('rent') }}">For Rent</a>
                            </div>
                        </div>

                        <h3>Team Home Office</h3>
                        <p>
                            Getting an office spacing is quite easy without much stress, check our listings
                        </p>
                    </div>
                </div>

{{--                <div class="col-md-3 col-sm-6">--}}
{{--                    <!-- Icon Box -->--}}
{{--                    <div class="icon-box-1">--}}

{{--                        <div class="icon-container">--}}
{{--                            <i class="im im-icon-Car-3"></i>--}}
{{--                            <div class="icon-links">--}}
{{--                                <a href="{{ route('sale') }}">For Sale</a>--}}
{{--                                <a href="{{ route('rent') }}">For Rent</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <h3>Shared Space</h3>--}}
{{--                        <p>--}}
{{--                            You can share an office space thereby saving cost and focus on what matters--}}
{{--                        </p>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-md-3 col-sm-6">--}}
{{--                    <!-- Icon Box -->--}}
{{--                    <div class="icon-box-1">--}}

{{--                        <div class="icon-container">--}}
{{--                            <i class="im im-icon-Clothing-Store"></i>--}}
{{--                            <div class="icon-links">--}}
{{--                                <a href="{{ route('sale') }}">For Sale</a>--}}
{{--                                <a href="{{ route('rent') }}">For Rent</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <h3>Co-Living</h3>--}}
{{--                        <p>--}}
{{--                            You can now share the burden of rent with another..--}}
{{--                        </p>--}}
{{--                    </div>--}}
{{--                </div>--}}

            </div>
        </div>
    </section>
    <!-- Fullwidth Section / End -->


    <!-- Container -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="headline centered margin-bottom-35 margin-top-10">Most Popular Places <span>Properties In Most Popular Places</span></h3>
            </div>
            <div class="col-md-6">
                <!-- Image Box -->
                <a href="#" class="img-box" data-background-image="images/lagos.jpeg">
                    <!-- Badge -->
                    <div class="listing-badges">
                        <span class="featured">Featured</span>
                    </div>

                    <div class="img-box-content visible">
                        <h4>Lagos </h4>
                        <span>24 Properties</span>
                    </div>
                </a>

            </div>
            <div class="col-md-6">
                <!-- Image Box -->
                <a href="#" class="img-box" data-background-image="images/abuja.jpeg">
                    <div class="img-box-content visible">
                        <h4>Abuja</h4>
                        <span>14 Properties</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!-- Container / End -->


    <!-- Fullwidth Section -->
    <section class="fullwidth margin-top-95 margin-bottom-0">

        <!-- Box Headline -->
        <h3 class="headline-box">Articles & Tips</h3>

        <div class="container">
            <div class="row">

                <div class="col-md-4">

                    <!-- Blog Post -->
                    <div class="blog-post">

                        <!-- Img -->
                        <a href="blog-post.html" class="post-img">
                            <img src="images/blog-post-01.jpg" alt="">
                        </a>

                        <!-- Content -->
                        <div class="post-content">
                            <h3><a href="#">8 Tips to Help You Finding New Home</a></h3>
                            <p>Nam nisl lacus, dignissim ac tristique ut, scelerisque eu massa. Vestibulum ligula nunc, rutrum in malesuada vitae. </p>

                            <a href="blog-post.html" class="read-more">Read More <i class="fa fa-angle-right"></i></a>
                        </div>

                    </div>
                    <!-- Blog Post / End -->

                </div>

                <div class="col-md-4">

                    <!-- Blog Post -->
                    <div class="blog-post">

                        <!-- Img -->
                        <a href="blog-post.html" class="post-img">
                            <img src="images/blog-post-02.jpg" alt="">
                        </a>

                        <!-- Content -->
                        <div class="post-content">
                            <h3><a href="#">Bedroom Colors You'll Never Regret</a></h3>
                            <p>Nam nisl lacus, dignissim ac tristique ut, scelerisque eu massa. Vestibulum ligula nunc, rutrum in malesuada vitae. </p>

                            <a href="blog-post.html" class="read-more">Read More <i class="fa fa-angle-right"></i></a>
                        </div>

                    </div>
                    <!-- Blog Post / End -->

                </div>

                <div class="col-md-4">

                    <!-- Blog Post -->
                    <div class="blog-post">

                        <!-- Img -->
                        <a href="blog-post.html" class="post-img">
                            <img src="images/blog-post-03.jpg" alt="">
                        </a>

                        <!-- Content -->
                        <div class="post-content">
                            <h3><a href="#">What to Do a Year Before Buying Apartment</a></h3>
                            <p>Nam nisl lacus, dignissim ac tristique ut, scelerisque eu massa. Vestibulum ligula nunc, rutrum in malesuada vitae. </p>

                            <a href="blog-post.html" class="read-more">Read More <i class="fa fa-angle-right"></i></a>
                        </div>

                    </div>
                    <!-- Blog Post / End -->

                </div>

            </div>
        </div>
    </section>
    <!-- Fullwidth Section / End -->


    <!-- Flip banner -->
    <a href="listings-half-map-grid-standard.html" class="flip-banner parallax" data-background="images/flip-banner-bg.jpg" data-color="#274abb" data-color-opacity="0.9" data-img-width="2500" data-img-height="1600">
        <div class="flip-banner-content">
            <h2 class="flip-visible">We help people and homes find each other</h2>
            <h2 class="flip-hidden">Browse Properties <i class="sl sl-icon-arrow-right"></i></h2>
        </div>
    </a>
    <!-- Flip banner / End -->

@endsection
