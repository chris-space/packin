@extends("layouts.app")

@section('head_title', 'Featured Properties | '.getcong('site_name') )
@section('head_url', Request::url())

@section("content")

    <!-- Titlebar
    ================================================== -->
    <div class="parallax titlebar"
         data-background="images/listings-parallax.jpg"
         data-color="#333333"
         data-color-opacity="0.7"
         data-img-width="800"
         data-img-height="505">

        <div id="titlebar">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <h2>Featured Properties</h2>

                        <!-- Breadcrumbs -->
                        <nav id="breadcrumbs">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li>properties</li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row sticky-wrapper">

            <div class="col-md-8">

                <!-- Main Search Input + filter -->
                @include('_particles.top_search_filter')


            <!-- Listings -->
            @include('_particles.property_listing')
            <!-- Listings Container / End -->


                <!-- begin:pagination -->
            @include('_particles.pagination', ['paginator' => $properties])
            <!-- end:pagination -->

            </div>


            <!-- Sidebar
            ================================================== -->
            @include('_particles.sidebar')
            <!-- Sidebar / End -->
        </div>
    </div>

@endsection
