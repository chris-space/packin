@extends("layouts.app")

@section('head_title', $property->property_name .' | '.getcong('site_name') )
@section('head_description', substr(strip_tags($property->description),0,200))
@section('head_image', asset('/upload/properties/'.$property->featured_image.'-b.jpg'))
@section('head_url', Request::url())

@section("content")
    <!-- Titlebar
================================================== -->
    <div id="titlebar" class="property-titlebar margin-bottom-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <a href="listings-list-with-sidebar.html" class="back-to-listings"></a>
                    <div class="property-title">
                        <h2>{{ $property->property_name }} <span class="property-badge">For {{ $property->property_purpose }}</span></h2>
                        <span>
						<a href="#location" class="listing-address">
							<i class="fa fa-map-marker"></i>
							{{ $property->address }}
						</a>
					</span>
                    </div>

                    <div class="property-pricing">
                        <div class="property-price">
                            {{getcong('currency_sign')}}
                            @if($property->property_purpose === 'Sale')
                                {{ number_format((float)$property->sale_price) }}
                            @else
                                {{ number_format((float)$property->rent_price) }}
                            @endif
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <div class="row margin-bottom-50">
            <div class="col-md-12">

                <!-- Slider -->
                <div class="property-slider default">
                    @if($property->property_images1)
                        <a href="{{ asset('upload/properties/'.$property->property_images1.'-b.jpg') }}" data-background-image="{{ asset('upload/properties/'.$property->property_images1.'-b.jpg') }}" class="item mfp-gallery"></a>
                    @endif
                    @if($property->property_images2)
                        <a href="{{ asset('upload/properties/'.$property->property_images2.'-b.jpg') }}" data-background-image="{{ asset('upload/properties/'.$property->property_images2.'-b.jpg') }}" class="item mfp-gallery"></a>
                    @endif
                    @if($property->property_images3)
                        <a href="{{ asset('upload/properties/'.$property->property_images3.'-b.jpg') }}" data-background-image="{{ asset('upload/properties/'.$property->property_images3.'-b.jpg') }}" class="item mfp-gallery"></a>
                    @endif
                    @if($property->property_images4)
                       <a href="{{ asset('upload/properties/'.$property->property_images4.'-b.jpg') }}" data-background-image="{{ asset('upload/properties/'.$property->property_images4.'-b.jpg') }}" class="item mfp-gallery"></a>
                    @endif
                    @if($property->property_images5)
                        <a href="{{ asset('upload/properties/'.$property->property_images5.'-b.jpg') }}" data-background-image="{{ asset('upload/properties/'.$property->property_images5.'-b.jpg') }}" class="item mfp-gallery"></a>
                    @endif
                </div>

                <!-- Slider Thumbs -->
                <div class="property-slider-nav">
                    @if($property->property_images1)
                        <div class="item"><img src="{{ asset('upload/properties/'.$property->property_images1.'-b.jpg') }}" alt=""></div>
                    @endif

                    @if($property->property_images2)
                        <div class="item"><img src="{{ asset('upload/properties/'.$property->property_images2.'-b.jpg') }}" alt=""></div>
                    @endif
                    @if($property->property_images3)
                        <div class="item"><img src="{{ asset('upload/properties/'.$property->property_images3.'-b.jpg') }}" alt=""></div>
                    @endif
                    @if($property->property_images4)
                        <div><img src="{{ asset('upload/properties/'.$property->property_images4.'-b.jpg') }}" alt=""></div>
                    @endif
                    @if($property->property_images5)
                        <div class="item">
                            <img src="{{ asset('upload/properties/'.$property->property_images5.'-b.jpg') }}" alt="">
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <!-- Property Description -->
            <div class="col-lg-8 col-md-7 sp-content">
                <div class="property-description">

                    <!-- Main Features -->
                    <ul class="property-main-features">
                        @if($property->area)
                            <li>Area <span>{{$property->area}} </span></li>
                        @endif
                        @if($property->bathrooms)
                             <li>Bathrooms <span>{{$property->bathrooms}}</span></li>
                         @endif
                        @if($property->bedrooms)
                             <li>Bedrooms <span>{{$property->bedrooms}}</span></li>
                         @endif
                    </ul>

                    <!-- Description -->
                    <h3 class="desc-headline">Description</h3>
                    <div class="show-more">
                        {{ $property->description }}

                        <a href="#" class="show-more-button">Show More <i class="fa fa-angle-down"></i></a>
                    </div>

                    <!-- Details -->
                    <h3 class="desc-headline">Details</h3>
                    <ul class="property-features margin-top-0">
                        <li>Building Age: <span>2 Years</span></li>
                        <li>Parking: <span>Attached Garage</span></li>
                        <li>Cooling: <span>Central Cooling</span></li>
                        <li>Heating: <span>Forced Air, Gas</span></li>
                        <li>Sewer: <span>Public/City</span></li>
                        <li>Water: <span>City</span></li>
                        <li>Exercise Room: <span>Yes</span></li>
                        <li>Storage Room: <span>Yes</span></li>
                    </ul>


                    <!-- Features -->
                    @if(isset($property->property_features))
                        <h3 class="desc-headline">Features</h3>
                        <ul class="property-features checkboxes margin-top-0">
                            @foreach(explode(',',$property->property_features) as $features)
                                <li>{{$features}}</li>
                            @endforeach
                        </ul>
                    @endif

                    <!-- Floorplans -->
                    <h3 class="desc-headline no-border">Floorplans</h3>
                    <!-- Accordion -->
                    <div class="style-1 fp-accordion">
                        <div class="accordion">

                            <h3>First Floor <span>460 sq ft</span> <i class="fa fa-angle-down"></i> </h3>
                            <div>
                                <a class="floor-pic mfp-image" href="https://i.imgur.com/kChy7IU.jpg">
                                    <img src="https://i.imgur.com/kChy7IU.jpg" alt="">
                                </a>
                                <p>Mauris mauris ante, blandit et, ultrices a, susceros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate aliquam egestas litora torquent conubia.</p>
                            </div>

                            <h3>Second Floor <span>440 sq ft</span> <i class="fa fa-angle-down"></i></h3>
                            <div>
                                <a class="floor-pic mfp-image" href="https://i.imgur.com/l2VNlwu.jpg">
                                    <img src="https://i.imgur.com/l2VNlwu.jpg" alt="">
                                </a>
                                <p>Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. Nullam laoreet, velit ut taciti sociosqu condimentum feugiat.</p>
                            </div>

                            <h3>Garage <span>140 sq ft</span> <i class="fa fa-angle-down"></i></h3>
                            <div>
                                <a class="floor-pic mfp-image" href="https://i.imgur.com/0zJYERy.jpg">
                                    <img src="https://i.imgur.com/0zJYERy.jpg" alt="">
                                </a>
                            </div>

                        </div>
                    </div>

                    <!-- Location -->
                    <h3 class="desc-headline no-border" id="location">Location</h3>
                    <div id="propertyMap-container">
                        <div id="propertyMap" data-latitude="40.7427837" data-longitude="-73.11445617675781"></div>
                        <a href="#" id="streetView">Street View</a>
                    </div>
                </div>
            </div>
            <!-- Property Description / End -->


            <!-- Sidebar -->
            <div class="col-lg-4 col-md-5 sp-sidebar">
                <div class="sidebar sticky right">

                    <!-- Widget -->
                    <div class="widget margin-bottom-30">
                        <button class="widget-button with-tip" data-tip-content="Print"><i class="sl sl-icon-printer"></i></button>
                        <button class="widget-button with-tip" data-tip-content="Add to Bookmarks"><i class="fa fa-star-o"></i></button>
                        <button class="widget-button with-tip compare-widget-button" data-tip-content="Add to Compare"><i class="icon-compare"></i></button>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Widget / End -->


                    <!-- Booking Widget -->
                    <div class="widget">
                        <div id="booking-widget-anchor" class="boxed-widget booking-widget margin-top-35">
                            <h3><i class="fa fa-calendar-check-o"></i> Schedule a Tour</h3>
                            <div class="row with-forms  margin-top-0">

                                <!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
                                <div class="col-lg-12">
                                    <input type="text" id="date-picker" placeholder="Date" readonly="readonly">
                                </div>

                                <!-- Panel Dropdown -->
                                <div class="col-lg-12">
                                    <div class="panel-dropdown time-slots-dropdown">
                                        <a href="#">Time</a>
                                        <div class="panel-dropdown-content padding-reset">
                                            <div class="panel-dropdown-scrollable">

                                                <!-- Time Slot -->
                                                <div class="time-slot">
                                                    <input type="radio" name="time-slot" id="time-slot-1">
                                                    <label for="time-slot-1">
                                                        <strong>8:30 am - 9:00 am</strong>
                                                        <span>1 slot available</span>
                                                    </label>
                                                </div>

                                                <!-- Time Slot -->
                                                <div class="time-slot">
                                                    <input type="radio" name="time-slot" id="time-slot-2">
                                                    <label for="time-slot-2">
                                                        <strong>9:00 am - 9:30 am</strong>
                                                        <span>2 slots available</span>
                                                    </label>
                                                </div>

                                                <!-- Time Slot -->
                                                <div class="time-slot">
                                                    <input type="radio" name="time-slot" id="time-slot-3">
                                                    <label for="time-slot-3">
                                                        <strong>9:30 am - 10:00 am</strong>
                                                        <span>1 slots available</span>
                                                    </label>
                                                </div>

                                                <!-- Time Slot -->
                                                <div class="time-slot">
                                                    <input type="radio" name="time-slot" id="time-slot-4">
                                                    <label for="time-slot-4">
                                                        <strong>10:00 am - 10:30 am</strong>
                                                        <span>3 slots available</span>
                                                    </label>
                                                </div>

                                                <!-- Time Slot -->
                                                <div class="time-slot">
                                                    <input type="radio" name="time-slot" id="time-slot-5">
                                                    <label for="time-slot-5">
                                                        <strong>13:00 pm - 13:30 pm</strong>
                                                        <span>2 slots available</span>
                                                    </label>
                                                </div>

                                                <!-- Time Slot -->
                                                <div class="time-slot">
                                                    <input type="radio" name="time-slot" id="time-slot-6">
                                                    <label for="time-slot-6">
                                                        <strong>13:30 pm - 14:00 pm</strong>
                                                        <span>1 slots available</span>
                                                    </label>
                                                </div>

                                                <!-- Time Slot -->
                                                <div class="time-slot">
                                                    <input type="radio" name="time-slot" id="time-slot-7">
                                                    <label for="time-slot-7">
                                                        <strong>14:00 pm - 14:30 pm</strong>
                                                        <span>1 slots available</span>
                                                    </label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Panel Dropdown / End -->

                            </div>

                            <!-- Book Now -->
                            <a href="#" class="button book-now fullwidth margin-top-5">Send Request</a>
                        </div>

                    </div>
                    <!-- Booking Widget / End -->


                    <!-- Widget -->
                    <div class="widget">

                        <!-- Agent Widget -->
                        <div class="agent-widget">
                            <div class="agent-title">
                                <div class="agent-photo"><img src="{{  asset('/') }}images/agent-avatar.jpg" alt="" /></div>
                                <div class="agent-details">
                                    <h4><a href="#">Jennie Wilson</a></h4>
                                    <span><i class="sl sl-icon-call-in"></i>(123) 123-456</span>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <input type="text" placeholder="Your Email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$">
                            <input type="text" placeholder="Your Phone">
                            <textarea>I'm interested in this property [ID 123456] and I'd like to know more details.</textarea>
                            <button class="button fullwidth margin-top-5">Send Message</button>
                        </div>
                        <!-- Agent Widget / End -->

                    </div>
                    <!-- Widget / End -->


                    <!-- Widget -->
                    <div class="widget">
                        <h3 class="margin-bottom-35">Featured Properties</h3>

                        <div class="listing-carousel outer">
                            <!-- Item -->
                            <div class="item">
                                <div class="listing-item compact">

                                    <a href="#" class="listing-img-container">

                                        <div class="listing-badges">
                                            <span class="featured">Featured</span>
                                            <span>For Sale</span>
                                        </div>

                                        <div class="listing-img-content">
                                            <span class="listing-compact-title">Eagle Apartments <i>$275,000</i></span>

                                            <ul class="listing-hidden-content">
                                                <li>Area <span>530 sq ft</span></li>
                                                <li>Rooms <span>3</span></li>
                                                <li>Beds <span>1</span></li>
                                                <li>Baths <span>1</span></li>
                                            </ul>
                                        </div>

                                        <img src="{{  asset('/') }}images/listing-01.jpg" alt="">
                                    </a>

                                </div>
                            </div>
                            <!-- Item / End -->

                            <!-- Item -->
                            <div class="item">
                                <div class="listing-item compact">

                                    <a href="#" class="listing-img-container">

                                        <div class="listing-badges">
                                            <span class="featured">Featured</span>
                                            <span>For Sale</span>
                                        </div>

                                        <div class="listing-img-content">
                                            <span class="listing-compact-title">Selway Apartments <i>$245,000</i></span>

                                            <ul class="listing-hidden-content">
                                                <li>Area <span>530 sq ft</span></li>
                                                <li>Rooms <span>3</span></li>
                                                <li>Beds <span>1</span></li>
                                                <li>Baths <span>1</span></li>
                                            </ul>
                                        </div>

                                        <img src="{{  asset('/') }}images/listing-02.jpg" alt="">
                                    </a>

                                </div>
                            </div>
                            <!-- Item / End -->

                            <!-- Item -->
                            <div class="item">
                                <div class="listing-item compact">

                                    <a href="#" class="listing-img-container">

                                        <div class="listing-badges">
                                            <span class="featured">Featured</span>
                                            <span>For Sale</span>
                                        </div>

                                        <div class="listing-img-content">
                                            <span class="listing-compact-title">Oak Tree Villas <i>$325,000</i></span>

                                            <ul class="listing-hidden-content">
                                                <li>Area <span>530 sq ft</span></li>
                                                <li>Rooms <span>3</span></li>
                                                <li>Beds <span>1</span></li>
                                                <li>Baths <span>1</span></li>
                                            </ul>
                                        </div>

                                        <img src="{{  asset('/') }}images/listing-03.jpg" alt="">
                                    </a>

                                </div>
                            </div>
                            <!-- Item / End -->
                        </div>

                    </div>
                    <!-- Widget / End -->

                </div>
            </div>
            <!-- Sidebar / End -->

        </div>
    </div>
@endsection

@section('js')
    <!-- Maps -->
    <script type="text/javascript" src="{{ asset('/') }}scripts/infobox.min.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}scripts/markerclusterer.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}scripts/maps.js"></script>

    <!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
    <script src="{{ asset('/') }}scripts/moment.min.js"></script>
    <script src="{{ asset('/') }}scripts/daterangepicker.js"></script>
    <script>
        // Calendar Init
        $(function() {
            $('#date-picker').daterangepicker({
                "opens": "left",
                singleDatePicker: true,

                // Disabling Date Ranges
                isInvalidDate: function(date) {
                    // Disabling Date Range
                    var disabled_start = moment('09/02/2018', 'MM/DD/YYYY');
                    var disabled_end = moment('09/06/2018', 'MM/DD/YYYY');
                    return date.isAfter(disabled_start) && date.isBefore(disabled_end);

                    // Disabling Single Day
                    // if (date.format('MM/DD/YYYY') == '08/08/2018') {
                    //     return true;
                    // }
                }
            });
        });

        // Calendar animation
        $('#date-picker').on('showCalendar.daterangepicker', function(ev, picker) {
            $('.daterangepicker').addClass('calendar-animated');
        });
        $('#date-picker').on('show.daterangepicker', function(ev, picker) {
            $('.daterangepicker').addClass('calendar-visible');
            $('.daterangepicker').removeClass('calendar-hidden');
        });
        $('#date-picker').on('hide.daterangepicker', function(ev, picker) {
            $('.daterangepicker').removeClass('calendar-visible');
            $('.daterangepicker').addClass('calendar-hidden');
        });
    </script>


    <!-- Replacing dropdown placeholder with selected time slot -->
    <script>
        $(".time-slot").each(function() {
            var timeSlot = $(this);
            $(this).find('input').on('change',function() {
                var timeSlotVal = timeSlot.find('strong').text();

                $('.panel-dropdown.time-slots-dropdown a').html(timeSlotVal);
                $('.panel-dropdown').removeClass('active');
            });
        });
    </script>

@endsection
