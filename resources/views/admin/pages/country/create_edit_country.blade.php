@extends("admin.admin_app")

@section("content")

    <div id="main">
        <div class="page-header">
            <h2> {{ isset($country->name) ? 'Edit: '. $country->name : 'Add Country' }}</h2>
            <a href="{{ route('admin.country') }}" class="btn btn-default-light btn-xs"><i class="md md-backspace"></i> Back</a>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::open(array('url' => route('admin.country.storeOrUpdate'),'class'=>'form-horizontal padding-15','name'=>'city_form','id'=>'city_form','role'=>'form','enctype' => 'multipart/form-data')) !!}
                <input type="hidden" name="id" value="{{ $country->id ?? null }}">
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Country Name</label><br />
                    <div class="col-sm-9">
                        <input type="text" name="name" value="{{ $country->name ?? old('name') }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="status" class="col-sm-3 control-label">Status</label><br />
                    <div class="col-sm-9">
                        <select id="status" class="form-control" name="status">
                            <option @if(isset($country->status) && $country->status === 'active') selected @endif value="active">Active</option>
                            <option @if(isset($country->status) && $country->status === 'inactive') selected @endif value="inactive">Inactive</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-md-offset-3 col-sm-9 ">
                        <button type="submit" class="btn btn-primary">{{ isset($country->id) ? 'Edit Country ' : 'Add Country' }}</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
