@extends("admin.admin_app")

@section("content")

    <div id="main">
        <div class="page-header">
            <h2> {{ isset($city->name) ? 'Edit: '. $city->name : 'Add City' }}</h2>
            <a href="{{ route('admin.city') }}" class="btn btn-default-light btn-xs"><i class="md md-backspace"></i> Back</a>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @endif
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::open(array('url' => route('admin.city.storeOrUpdate'),'class'=>'form-horizontal padding-15','name'=>'city_form','id'=>'city_form','role'=>'form','enctype' => 'multipart/form-data')) !!}
                <input type="hidden" name="id" value="{{ $city->id ?? old('id') }}">
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">City Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" value="{{ $city->name ?? old('name') }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="country" class="col-sm-3 control-label">Country</label><br />
                    <div class="col-sm-9">
                        <select id="country" name="country" class="selectpicker show-tick form-control">
                            <option value="">Select Country</option>
                            @if(isset($countries))
                                @foreach($countries as $country)
                                    @if(isset($state))
                                        <option {{ $country->id === $city->state->country->country_id ? 'selected': '' }} value="{{ $country->id }}">{{ $country->name }}</option>
                                    @else
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="state" class="col-sm-3 control-label">State</label><br />
                    <div class="col-sm-9">
                        <select id="state" name="state" class="form-control show-tick form-controlselectpicker show-tick ">
                            <option value="">Select State</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="status" class="col-sm-3 control-label">Status</label><br />
                    <div class="col-sm-9">
                        <select id="status" class="form-control" name="status">
                            <option @if(isset($state->status) && $state->status === 'active') selected @endif value="active">Active</option>
                            <option @if(isset($state->status) && $state->status === 'inactive') selected @endif value="inactive">Inactive</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-md-offset-3 col-sm-9 ">
                        <button type="submit" class="btn btn-primary">{{ isset($city->id) ? 'Edit City ' : 'Add City' }}</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        // Start JQuery integration
        $(document).ready(function(){
            $('#country').on('change', function(){
                // Department id
                var id = $(this).val();

                // Empty the dropdown
                $('#state').find('option').not(':first').remove();

                var url = '{{ route("state.byCountry", ":cid") }}';
                url = url.replace(':cid', id);
                $("#state").html('<option> Please wait ...</option>');

                // AJAX request
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    beforeSend: function() {
                        $("#state").html('<option> Please wait ...</option>');
                        $("#state").prop('disabled', true); // disable button
                    },
                    success: function(response){

                        $("#state").html("<option value='0'> Select State ...</option>");
                        var len = 0;
                        if(response != null){
                            len = response.length;
                        }

                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++){
                                var id = response[i].id;
                                var name = response[i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#state").append(option);
                                $("#state").prop('disabled', false); // disable button
                            }
                        }

                    }
                });
            });
        });
    </script>
@endsection
