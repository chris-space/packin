@extends("admin.admin_app")

@section("content")
    <div id="main">
        <div class="page-header">

            <div class="pull-right">
                <a href="{{ route('admin.state.create') }}" class="btn btn-primary">Add State <i class="fa fa-plus"></i></a>
            </div>
            <h2>States</h2>
        </div>
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_message') }}
            </div>
        @endif

        <div class="panel panel-default panel-shadow">
            <div class="panel-body">
                <table id="data-table" class="table table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Name</th>
                        <th>Cities added</th>
                        <th>Country</th>
                        <th>Status</th>
                        <th class="text-center width-100">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($states as $state)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $state->name }}</td>
                            <td>{{ $state->cities->count() }}</td>
                            <td>{{ $state->country->name }}</td>
                            <td>{{ $state->status }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Actions <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                        <li>
                                            @if($state->status == 'active')
                                                <a href="{{ route('admin.state.status', ['id' => $state->id]) }}"><i class="md md-close"></i> Unpublish</a>
                                            @else
                                                <a href="{{ route('admin.state.status', ['id' => $state->id]) }}"><i class="md md-check"></i> Publish</a>
                                            @endif
                                        </li>
                                        <li><a href="{{ route('admin.state.edit', ['id' => $state->id]) }}"><i class="md md-edit"></i> Edit Editor</a></li>
                                        <li><a href="{{ route('admin.state.delete', ['id' => $state->id]) }}"><i class="md md-delete"></i> Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@endsection
