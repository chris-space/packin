@if ($paginator->lastPage() > 1)
<div class="pagination-container margin-top-20">
    <nav class="pagination">
        <ul>
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a href="{{ $paginator->url($i) }}" class="{{ ($paginator->currentPage() == $i) ? ' current-page' : '' }}">{{ $i }}</a>
                </li>
            @endfor
{{--            <li class="blank">...</li>--}}
{{--            <li><a href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a></li>--}}
        </ul>
    </nav>

    <nav class="pagination-next-prev">
        <ul>
            <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                <a href="{{ $paginator->url(1) }}" class="prev">Previous</a>
            </li>
            <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                <a href="{{ $paginator->url($paginator->currentPage()+1) }}" class="next">Next</a>
            </li>
        </ul>
    </nav>
</div>
@endif

