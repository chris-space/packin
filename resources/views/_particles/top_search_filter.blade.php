<!-- Main Search Input -->
<form method="post" action="{{ route('property.search') }}">
    @csrf
    <div class="main-search-input margin-bottom-35">
        <input type="text" name="terms" class="ico-01" placeholder="Enter address e.g. street, city and state" value=""/>
        <button class="button">Search</button>
    </div>
</form>

<!-- Sorting / Layout Switcher -->
<div class="row margin-bottom-15">

    <div class="col-md-6">
        <!-- Sort by -->
        <div class="sort-by">
            <label>Sort by:</label>

            <div class="sort-by-select">
                <select data-placeholder="Default order" class="chosen-select-no-single" >
                    <option>Default Order</option>
                    <option>Price Low to High</option>
                    <option>Price High to Low</option>
                    <option>Newest Properties</option>
                    <option>Oldest Properties</option>
                </select>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <!-- Layout Switcher -->
        <div class="layout-switcher">
            <a href="#" class="list"><i class="fa fa-th-list"></i></a>
            <a href="#" class="grid"><i class="fa fa-th-large"></i></a>
        </div>
    </div>
</div>
