<!-- Footer
    ================================================== -->
<div id="footer" class="sticky-footer">
    <!-- Main -->
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-6">
                @if(getcong('site_logo')) <img class="footer-logo" src="{{ asset('upload/'.getcong('site_logo')) }}" alt="{{ config('app.name') }}"> @else {{getcong('site_name')}} @endif
                <br><br>
                <p>
                    Packin is a property/ Real estate company with a unique approach to tackling a new age challenge
                    At pack-in, we are connecting the self-employed, freelancers, micro-workers and other professionals helping them find spaces to work in,
                    in the areas they couldn't previously afford, and may not need full time.
                </p>
            </div>

            <div class="col-md-4 col-sm-6 ">
                <h4>Helpful Links</h4>
                <ul class="footer-links">
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Sign Up</a></li>
                    <li><a href="{{ route('admin.dashboard') }}">My Account</a></li>
                    <li><a href="{{ route('admin.property.create') }}">Add Property</a></li>
{{--                    <li><a href="#">Pricing</a></li>--}}
                    <li><a href="#">Privacy Policy</a></li>
                </ul>

                <ul class="footer-links">
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Our Agents</a></li>
                    <li><a href="#">How It Works</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="col-md-3  col-sm-12">
                <h4>Contact Us</h4>
                <div class="text-widget">
                    <span>No 22 Kigoma Street Wuse Zone 7, Abuja</span> <br>
                    Phone: <span>+2348129070463, +2348108590754,+2347035991271 </span><br>
                    E-Mail:<span> <a href="#">info@packin.com.ng</a> </span><br>
                </div>

                <ul class="social-icons margin-top-20">
                    <li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
                    <li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
                    <li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
                    <li><a class="vimeo" href="#"><i class="icon-vimeo"></i></a></li>
                </ul>

            </div>

        </div>

        <!-- Copyright -->
        <div class="row">
            <div class="col-md-12">
                <div class="copyrights">{{getcong('site_copyright')}}</div>
            </div>
        </div>

    </div>

</div>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>
