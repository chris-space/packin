<!-- Header Container
================================================== -->
<header id="header-container">

    <!-- Topbar -->
    <div id="top-bar">
        <div class="container">

            <!-- Left Side Content -->
            <div class="left-side">

                <!-- Top bar -->
                <ul class="top-bar-menu">
                    <li><i class="fa fa-phone"></i> +2348129070463 </li>
                    <li><i class="fa fa-envelope"></i> <a href="#">info@packin.com.ng</a></li>
                </ul>

            </div>
            <!-- Left Side Content / End -->


            <!-- Left Side Content -->
            <div class="right-side">

                <!-- Social Icons -->
                <ul class="social-icons">
                    <li><a class="facebook" href="{{getcong('social_facebook')}}"><i class="icon-facebook"></i></a></li>
                    <li><a class="twitter" href="{{getcong('social_twitter')}}"><i class="icon-twitter"></i></a></li>
                    <li><a class="gplus" href="{{getcong('social_gplus')}}"><i class="icon-gplus"></i></a></li>
                    <li><a class="instagram" href="{{getcong('social_linkedin')}}"><i class="icon-instagram"></i></a></li>
                </ul>

            </div>
            <!-- Left Side Content / End -->

        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Topbar / End -->

    <!-- Header -->
    <div id="header">
        <div class="container">
            <!-- Left Side Content -->
            <div class="left-side">
                <!-- Logo -->
                <div id="logo">
                    <a href="{{ url('/') }}">
                        @if(getcong('site_logo')) <img src="{{ asset('upload/'.getcong('site_logo')) }}" alt=""> @else {{getcong('site_name')}} @endif
                    </a>
                </div>
                <!-- Mobile Navigation -->
                <div class="mmenu-trigger">
                    <button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
                    </button>
                </div>

                <!-- Main Navigation -->
                <nav id="navigation" class="style-1">
                    <ul id="responsive">
                        <li><a class="{{classActivePathPublic('')}}" href="{{ url('/') }}">Home</a></li>
                        <li><a class="{{classActivePathPublic('properties')}}" href="{{ route('properties') }}">Properties</a></li>
                        <li><a class="{{classActivePathPublic('featured')}}" href="{{ route('featured') }}">Featured</a></li>
                        <li><a class="{{classActivePathPublic('rent')}}" href="{{ route('rent') }}">Rent</a></li>
                        <li><a class="{{classActivePathPublic('sale')}}" href="{{ route('sale') }}">Sale</a></li>
{{--                        <li><a class="{{classActivePathPublic('agents')}}" href="{{ route('agents') }}">Agents</a></li>--}}
                    </ul>
                </nav>
                <div class="clearfix"></div>
                <!-- Main Navigation / End -->

            </div>
            <!-- Left Side Content / End -->

            <!-- Right Side Content / End -->
            <div class="right-side">
                <!-- Header Widget -->
                <div class="header-widget">
                    @guest
                        <a href="{{ route('login') }}" class="sign-in"><i class="fa fa-user"></i> Log In / Register</a>
                    @else
                        <a href="{{ route('admin.dashboard') }}" class="sign-in"><i class="fa fa-user"></i> Dashboard</a>
                    @endguest
                    <a href="{{ route('admin.property.create') }}" class="button border">Submit Property</a>
                </div>
                <!-- Header Widget / End -->
            </div>
            <!-- Right Side Content / End -->

        </div>
    </div>
    <!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->
