<div class="listings-container list-layout">

    @if(isset($properties) && $properties->count() > 0)
        @foreach($properties as $i => $property)
            <!-- Listing Item -->
                <div class="listing-item">

                    <a href="{{ route('property.view', ['slug' => $property->property_slug]) }}" class="listing-img-container">

                        <div class="listing-badges">
                            @if($property->featured_property === 1)
                                <span class="featured">Featured</span>
                            @endif
                            <span>For {{$property->property_purpose}}</span>
                        </div>

                        <div class="listing-img-content">
                                    <span class="listing-price">
                                        {{env('Currency_Code')}}
                                        @if($property->property_purpose === 'Sale')
                                        {{ number_format((float)$property->sale_price) }}
                                        @else
                                        {{ number_format((float)$property->rent_price) }}
                                        @endif
                                        </i></span>
                            <span class="like-icon with-tip" data-tip-content="Add to Bookmarks"></span>
                        </div>

                        <div class="listing-carousel">
                            @if($property->property_images1)
                                <div><img src="{{ asset('upload/properties/'.$property->property_images1.'-b.jpg') }}" alt=""></div>
                            @endif

                            @if($property->property_images2)
                                <div class="item"><img src="{{ asset('upload/properties/'.$property->property_images2.'-b.jpg') }}" alt=""></div>
                            @endif
                            @if($property->property_images3)
                                <div><img src="{{ asset('upload/properties/'.$property->property_images3.'-b.jpg') }}" alt=""></div>
                            @endif
                            @if($property->property_images4)
                                <div><img src="{{ asset('upload/properties/'.$property->property_images4.'-b.jpg') }}" alt=""></div>
                            @endif
                            @if($property->property_images5)
                                <div>
                                    <img src="{{ asset('upload/properties/'.$property->property_images5.'-b.jpg') }}" alt="">
                                </div>
                            @endif
                        </div>
                    </a>

                    <div class="listing-content">

                        <div class="listing-title">
                            <h4><a href="{{ route('property.view', ['slug' => $property->property_slug]) }}">{{ Str::limit($property->property_name,35) }}</a></h4>
                            <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
                                <i class="fa fa-map-marker"></i>
                                {{ Str::limit($property->address,40) }}
                            </a>
                        </div>

                        <ul class="listing-details">
                            @if($property->area)
                                <li>Area <span>{{$property->area}} </span></li>
                            @endif
                            @if($property->bedrooms)
                                <li>Bedrooms <span>{{$property->bedrooms}}</span></li>
                            @endif
                            @if($property->bathrooms)
                                <li>Bathrooms <span>{{$property->bathrooms}}</span></li>
                            @endif
                        </ul>

                        <div class="listing-footer">
                            <a href="#"><i class="fa fa-user"></i> {{ $property->user->name }}</a>
                            <span><i class="fa fa-calendar-o"></i> {{ $property->created_at->diffForHumans() }}</span>
                        </div>

                    </div>

                </div>
                <!-- Listing Item / End -->
        @endforeach
        @else
        <h4>No property listing..</h4>
    @endif

</div>
