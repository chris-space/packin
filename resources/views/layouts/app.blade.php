<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="description" content="@yield('head_description', getcong('site_description'))">
    <meta property="keywords" content="@yield('head_keywords', getcong('site_keywords'))" />

    <meta property="og:type" content="article"/>
    <meta property="og:title" content="@yield('head_title',  getcong('site_name'))"/>
    <meta property="og:description" content="@yield('head_description', getcong('site_description'))"/>

    <meta property="og:image" content="@yield('head_image', url('/upload/logo.png'))" />
    <meta property="og:url" content="@yield('head_url', url('/'))" />

    <link href="{{ asset('upload/'.getcong('site_favicon')) }}" rel="icon" type="image/x-icon" />

    <title>@yield('head_title', getcong('site_name'))</title>

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/color.css') }}">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="{{ asset('assets/js/html5shiv.js') }}"></script>
      <script src="{{ asset('assets/js/respond.min.js') }}"></script>
    <![endif]-->

    @yield('css')

    {!!getcong('site_header_code')!!}

    {!! getcong('addthis_share_code')!!}

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

    @include("_particles.header")

        @yield('content')

    @include("_particles.footer")


    <!-- Scripts
    ================================================== -->
    <script type="text/javascript" src="{{ asset('scripts/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/jquery-migrate-3.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/chosen.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/magnific-popup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/rangeSlider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/sticky-kit.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/masonry.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/mmenu.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/tooltips.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('scripts/custom.js') }}"></script>

    <!-- Google Autocomplete -->
    <script>
        function initAutocomplete() {
            var input = document.getElementById('autocomplete-input');
            var autocomplete = new google.maps.places.Autocomplete(input);

            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete"></script>
        @yield('js')
    {!!getcong('site_footer_code')!!}
</div>
<!-- Wrapper / End -->
</body>
</html>
